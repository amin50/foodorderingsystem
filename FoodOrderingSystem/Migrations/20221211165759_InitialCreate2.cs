﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FoodOrderingSystem.Migrations
{
    public partial class InitialCreate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_FoodOrders_order",
                table: "FoodOrders",
                column: "order");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodOrders_Orders_order",
                table: "FoodOrders",
                column: "order",
                principalTable: "Orders",
                principalColumn: "orderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodOrders_Orders_order",
                table: "FoodOrders");

            migrationBuilder.DropIndex(
                name: "IX_FoodOrders_order",
                table: "FoodOrders");
        }
    }
}
