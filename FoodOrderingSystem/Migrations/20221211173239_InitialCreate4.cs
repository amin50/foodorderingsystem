﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FoodOrderingSystem.Migrations
{
    public partial class InitialCreate4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_DrinkOrders_drink",
                table: "DrinkOrders",
                column: "drink");

            migrationBuilder.CreateIndex(
                name: "IX_DrinkOrders_order",
                table: "DrinkOrders",
                column: "order");

            migrationBuilder.AddForeignKey(
                name: "FK_DrinkOrders_Drinks_drink",
                table: "DrinkOrders",
                column: "drink",
                principalTable: "Drinks",
                principalColumn: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_DrinkOrders_Orders_order",
                table: "DrinkOrders",
                column: "order",
                principalTable: "Orders",
                principalColumn: "orderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DrinkOrders_Drinks_drink",
                table: "DrinkOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_DrinkOrders_Orders_order",
                table: "DrinkOrders");

            migrationBuilder.DropIndex(
                name: "IX_DrinkOrders_drink",
                table: "DrinkOrders");

            migrationBuilder.DropIndex(
                name: "IX_DrinkOrders_order",
                table: "DrinkOrders");
        }
    }
}
