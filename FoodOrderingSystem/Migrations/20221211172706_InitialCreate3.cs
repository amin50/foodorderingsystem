﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FoodOrderingSystem.Migrations
{
    public partial class InitialCreate3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_FoodOrders_food",
                table: "FoodOrders",
                column: "food");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodOrders_Foods_food",
                table: "FoodOrders",
                column: "food",
                principalTable: "Foods",
                principalColumn: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodOrders_Foods_food",
                table: "FoodOrders");

            migrationBuilder.DropIndex(
                name: "IX_FoodOrders_food",
                table: "FoodOrders");
        }
    }
}
