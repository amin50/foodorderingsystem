﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FoodOrderingSystem.Migrations
{
    public partial class InitialCreate6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Users_role",
                table: "Users",
                column: "role");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Roles_role",
                table: "Users",
                column: "role",
                principalTable: "Roles",
                principalColumn: "roleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Roles_role",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_role",
                table: "Users");
        }
    }
}
