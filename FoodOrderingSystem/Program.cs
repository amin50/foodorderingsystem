using System.Net;
using System.Text;
using FoodOrderingSystem.Services.Implementation;
using FoodOrderingSystem.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using TtsAPI.Config.Data;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        Description = "Standard Authorization header using the Bearer scheme (\"bearer {token}\")",
        In = ParameterLocation.Header,
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey
    });
    options.OperationFilter<SecurityRequirementsOperationFilter>();
});

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                .GetBytes(builder.Configuration.GetSection("AppSettings:Token").Value)),
            ValidateIssuer = false,
            ValidateAudience = false
        };
    });

builder.Services.AddDbContext<DBContext>(options =>
{
    var config = builder.Configuration;

    string server = config.GetSection("DatabaseConnection:Server").Value;
    string database = config.GetSection("DatabaseConnection:Database").Value;
    string userId = config.GetSection("DatabaseConnection:UserId").Value;
    string password = config.GetSection("DatabaseConnection:Password").Value;

    var connectionString = $"Server={server};Database={database};User Id={userId};Password={password};";
    options.UseSqlServer(connectionString);
});

builder.Services.AddScoped<IFood, ImplFood>();
builder.Services.AddScoped<IDrink, ImplDrink>();
builder.Services.AddScoped<IOrder, ImplOrder>();
builder.Services.AddScoped<IUser, ImplUser>();
builder.Services.AddScoped<IJWT, ImplJWT>();

var app = builder.Build();

app.Use(async (context, next) =>
{
    await next();

    if (context.Response.StatusCode == (int)HttpStatusCode.Unauthorized)
    {
        await context.Response.WriteAsync("Token Validation Has Failed. Request Access Denied");
    }
});

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


// app.UseHttpsRedirection();
app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
