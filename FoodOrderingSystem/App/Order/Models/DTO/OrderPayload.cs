
using FoodOrderingSystem.Models.Entities;
using System.ComponentModel.DataAnnotations;

namespace FoodOrderingSystem.Models.DTO
{
    public class OrderPayload
    {
        public int orderId {get;set;}
        [MaxLength(50)]
        public string? tableNumber {get;set;}
        
        [MaxLength(512)]
        public string? orderNumber {get;set;}
        // public bool? isActive {get;set;}
        public double? total {get;set;}

        public List<FoodOrderPayload>? foodOrders {get;set;}
        public List<DrinkOrderPayload>? drinkOrders {get;set;}
    }
}