using System.ComponentModel.DataAnnotations;

namespace FoodOrderingSystem.Models.Entities
{
    public class Files
    {
        [Key]
        public string Id { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public byte[] Content { get; set; }
        public Types? type { get; set; }
    }
}

public enum Types
{
    INT = 1,
    NVARCHAR = 2,
    DOUBLE = 3,
    DATETIME = 4
}