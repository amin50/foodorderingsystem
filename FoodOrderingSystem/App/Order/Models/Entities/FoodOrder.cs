
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
// using Newtonsoft.Json;

namespace FoodOrderingSystem.Models.Entities

{
    public class FoodOrder
    {
        [Key]
        public int foodOrderId {get;set;}
        public int? food {get;set;}
        // [JsonIgnore]
        public Food? foodObj {get;set;}
        public DateTime? created_at {get;set;} = DateTime.Now;
        public DateTime? updated_at {get;set;} = DateTime.Now;

        public int? order { get; set; }
        [JsonIgnore]
        public Order? orderObj { get; set; }

    }
}