
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
// using Newtonsoft.Json;

namespace FoodOrderingSystem.Models.Entities
{
    public class DrinkOrder
    {
        [Key]
        public int drinkOrderId {get;set;}
        public int? drink {get;set;}
        // [JsonIgnore]
        public Drink? drinkObj {get;set;}
        public DateTime? created_at {get;set;} = DateTime.Now;
        public DateTime? updated_at {get;set;} = DateTime.Now;

        public int? order { get; set; }
        [JsonIgnore]
        public Order? orderObj { get; set; }
    }
}