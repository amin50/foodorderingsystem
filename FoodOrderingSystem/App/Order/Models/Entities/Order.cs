
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FoodOrderingSystem.Models.Entities
{
    public class Order
    {
        [Key]
        public int orderId {get;set;}
        [MaxLength(50)]
        public string? tableNumber {get;set;}
        // public List<DrinkOrder>? drinkOrders {get;set;}
        [MaxLength(512)]
        public string? orderNumber {get;set;}
        public bool? isActive {get;set;}
        public double? total {get;set;}
        public int? cashier {get;set;}//user kasir
        public int? waiter {get;set;}//user pelayan
        public DateTime? created_at {get;set;} = DateTime.Now;
        public DateTime? updated_at {get;set;} = DateTime.Now;

        // [JsonIgnore]
        public List<FoodOrder>? foodOrders {get;set;}
        public List<DrinkOrder>? drinkOrders {get;set;}
    }
}