using Microsoft.EntityFrameworkCore;
using FoodOrderingSystem.Models.Entities;

namespace FoodOrderingSystem.Models.Config
{
    public class FoodOrderConfig : IEntityTypeConfiguration<FoodOrder>
    {

        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<FoodOrder> builder)
        {
            builder
                .HasOne<Order>(s => s.orderObj)
                .WithMany(g => g.foodOrders)
                .HasForeignKey(s => s.order)
                .OnDelete(DeleteBehavior.ClientNoAction);
            builder
                .HasOne<Food>(s => s.foodObj)
                .WithMany(g => g.foodOrders)
                .HasForeignKey(s => s.food)
                .OnDelete(DeleteBehavior.ClientNoAction);
        }
    }
}