using Microsoft.EntityFrameworkCore;
using FoodOrderingSystem.Models.Entities;

namespace FoodOrderingSystem.Models.Config
{
    public class DrinkOrderConfig : IEntityTypeConfiguration<DrinkOrder>
    {

        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<DrinkOrder> builder)
        {
            builder
                .HasOne<Order>(s => s.orderObj)
                .WithMany(g => g.drinkOrders)
                .HasForeignKey(s => s.order)
                .OnDelete(DeleteBehavior.ClientNoAction);
            builder
                .HasOne<Drink>(s => s.drinkObj)
                .WithMany(g => g.drinkOrders)
                .HasForeignKey(s => s.drink)
                .OnDelete(DeleteBehavior.ClientNoAction);
        }
    }
}