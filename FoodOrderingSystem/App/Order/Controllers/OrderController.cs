using FoodOrderingSystem.Models.DTO;
using FoodOrderingSystem.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace FoodOrderingSystem.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : ControllerBase
    {
        private IOrder _Order;
        private IJWT _jwt;

        public OrderController(IOrder Order
        , IJWT jwt
        )
        {
            _Order = Order;
            _jwt = jwt;
        }

        [HttpPost]
        public IActionResult Create(OrderPayload body)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                var user = _jwt.VerifyUser(identity);
                if (user == null)
                {
                    return Unauthorized("Token Validation Has Failed. Request Access Denied");
                }

                var obj = _Order.Create(body, user.userId);
                return Ok(new
                {
                    StatusCode = 200,
                    message = "Create success",
                    Response = obj
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("Create Obj Error : " + e);
                return BadRequest(new
                {
                    StatusCode = 500,
                    Response = e
                });
            }
        }

        [HttpGet("detail")]
        public IActionResult Detail(int id)
        {
            try{
                var detail = _Order.Detail(id);
                return Ok(new
                {
                    StatusCode = 200,
                    message = "success",
                    Response = detail
                });
            }catch(Exception e)
            {
                return BadRequest(new
                {
                    StatusCode = 500,
                    Response = e
                });
            }
        }
    }
}