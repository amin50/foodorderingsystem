
using FoodOrderingSystem.Models.DTO;
using FoodOrderingSystem.Models.Entities;
using FoodOrderingSystem.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using TtsAPI.Config.Data;

namespace FoodOrderingSystem.Services.Implementation
{
    public class ImplOrder : IOrder
    {
        private DBContext _context;
        public ImplOrder(DBContext context)
        {
            _context = context;
        }

        public Order Create(OrderPayload body, int id)
        {
            Order obj = new Order();
            obj.tableNumber = body.tableNumber;
            obj.orderNumber = body.orderNumber;
            obj.isActive = true;
            obj.total = body.total;

            // obj.cashier = body.cashier;
            obj.waiter = id;

            _context.Orders.Add(obj);
            _context.SaveChanges();

            body.foodOrders.ForEach(c =>
            {
                FoodOrder o = new FoodOrder();
                o.food = c.food;
                o.order = obj.orderId;

                _context.FoodOrders.Add(o);
                _context.SaveChanges();
            });

            body.drinkOrders.ForEach(c =>
            {
                DrinkOrder o = new DrinkOrder();
                o.drink = c.drink;
                o.order = obj.orderId;

                _context.DrinkOrders.Add(o);
                _context.SaveChanges();
            });

            return obj;
        }

        public dynamic Detail(int id)
        {
            var b  =  _context.Orders
                .Include(e => e.foodOrders)
                .Include(d=> d.drinkOrders)
                .SingleOrDefault(x=> x.orderId == id);

            return b;
        }
    }
}