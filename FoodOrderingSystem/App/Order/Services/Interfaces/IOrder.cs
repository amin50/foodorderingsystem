
using FoodOrderingSystem.Models.DTO;
using FoodOrderingSystem.Models.Entities;

namespace FoodOrderingSystem.Services.Interfaces
{
    public interface IOrder
    {
        Order Create(OrderPayload body, int id);
        dynamic Detail(int id);
    }
}