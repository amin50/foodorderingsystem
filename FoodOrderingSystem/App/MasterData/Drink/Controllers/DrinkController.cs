using FoodOrderingSystem.Models.Entities;
using FoodOrderingSystem.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace FoodOrderingSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DrinkController : ControllerBase
    {
        private IDrink _Drink;

        public DrinkController(IDrink Drink)
        {
            _Drink = Drink;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try{
                var Drink = _Drink.GetAll();
                return Ok(new
                {
                    StatusCode = 200,
                    message = "Get success",
                    Response = Drink
                });
            }catch(Exception e)
            {
                return BadRequest(new
                {
                    StatusCode = 500,
                    Response = e
                });
            }
        }

        [HttpPost]
        public IActionResult Create(Drink body)
        {
            try
            {
                // var identity = HttpContext.User.Identity as ClaimsIdentity;
                // var user = _jwt.VerifyUser(identity);
                // if (user == null)
                // {
                //     return Unauthorized("Token Validation Has Failed. Request Access Denied");
                // }
                var obj = _Drink.Create(body);
                return Ok(new
                {
                    StatusCode = 200,
                    message = "Create success",
                    Response = obj
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("Create Obj Error : " + e);
                return BadRequest(new
                {
                    StatusCode = 500,
                    Response = e
                });
            }
        }
    }
}