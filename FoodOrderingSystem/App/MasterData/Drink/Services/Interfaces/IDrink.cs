
using FoodOrderingSystem.Models.Entities;

namespace FoodOrderingSystem.Services.Interfaces
{
    public interface IDrink
    {
        Drink Create(Drink body);
        IEnumerable<Drink> GetAll();
    }
}