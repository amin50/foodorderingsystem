using FoodOrderingSystem.Models.Entities;
using FoodOrderingSystem.Services.Interfaces;
using TtsAPI.Config.Data;

namespace FoodOrderingSystem.Services.Implementation
{
    public class ImplDrink: IDrink
    {
        private DBContext _context;
        public ImplDrink(DBContext context)
        {
            _context = context;
        }

        public Drink Create(Drink body)
        {
            _context.Drinks.Add(body);
            _context.SaveChanges();

            return body;
        }

        public IEnumerable<Drink> GetAll()
        {
            var listobj = _context.Drinks;

            return listobj;
        }
    }
}