using FoodOrderingSystem.Models.Entities;
using FoodOrderingSystem.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace FoodOrderingSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FoodController : ControllerBase
    {
        private IFood _Food;

        public FoodController(IFood Food)
        {
            _Food = Food;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try{
                var Food = _Food.GetAll();
                return Ok(new
                {
                    StatusCode = 200,
                    message = "Get success",
                    Response = Food
                });
            }catch(Exception e)
            {
                return BadRequest(new
                {
                    StatusCode = 500,
                    Response = e
                });
            }
        }

        [HttpPost]
        public IActionResult Create(Food body)
        {
            try
            {
                // var identity = HttpContext.User.Identity as ClaimsIdentity;
                // var user = _jwt.VerifyUser(identity);
                // if (user == null)
                // {
                //     return Unauthorized("Token Validation Has Failed. Request Access Denied");
                // }
                var obj = _Food.Create(body);
                return Ok(new
                {
                    StatusCode = 200,
                    message = "Create success",
                    Response = obj
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("Create Obj Error : " + e);
                return BadRequest(new
                {
                    StatusCode = 500,
                    Response = e
                });
            }
        }
    }
}