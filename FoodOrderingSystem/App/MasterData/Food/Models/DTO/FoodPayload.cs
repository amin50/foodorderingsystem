
namespace FoodOrderingSystem.Models.DTO
{
    public class FoodPayload
    {
        public string? name {get;set;}
        public double? price {get;set;}
        public bool isReady {get;set;}
    }
}