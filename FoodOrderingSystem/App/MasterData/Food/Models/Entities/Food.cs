

using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
// using Newtonsoft.Json;

namespace FoodOrderingSystem.Models.Entities
{
    public class Food
    {
        [Key]
        public int id {get;set;}
        [MaxLength(50)]
        public string? name {get;set;}
        public double? price {get;set;}
        public bool? isReady {get;set;}
        public DateTime? created_at {get;set;} = DateTime.Now;
        public DateTime? updated_at {get;set;} = DateTime.Now;
        [JsonIgnore]
        public List<FoodOrder>? foodOrders {get;set;}
    }
}