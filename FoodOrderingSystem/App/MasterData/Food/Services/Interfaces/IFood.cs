
using FoodOrderingSystem.Models.DTO;
using FoodOrderingSystem.Models.Entities;

namespace FoodOrderingSystem.Services.Interfaces
{
    public interface IFood
    {
        Food Create(Food body);
        IEnumerable<Food> GetAll();
    }
}