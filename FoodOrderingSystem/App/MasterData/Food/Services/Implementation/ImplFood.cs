using FoodOrderingSystem.Models.DTO;
using FoodOrderingSystem.Models.Entities;
using FoodOrderingSystem.Services.Interfaces;
using TtsAPI.Config.Data;

namespace FoodOrderingSystem.Services.Implementation
{
    public class ImplFood : IFood
    {
        private DBContext _context;
        public ImplFood(DBContext context)
        {
            _context = context;
        }

        public Food Create(Food body)
        {            
            _context.Foods.Add(body);
            _context.SaveChanges();

            return body;
        }

        public IEnumerable<Food> GetAll()
        {
            var listobj = _context.Foods;

            return listobj;
        }
    }
}