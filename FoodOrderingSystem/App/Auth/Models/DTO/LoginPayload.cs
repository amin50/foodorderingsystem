using System.ComponentModel.DataAnnotations;

namespace FoodOrderingSystem.Models.DTO
{
    public class LoginPayload
    {

        [MaxLength(255, ErrorMessage = "Too much characters")]
        public string username { get; set; }
        public string password { get; set; }
    }
}