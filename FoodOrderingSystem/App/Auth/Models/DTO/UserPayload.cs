
using System.ComponentModel.DataAnnotations;

namespace FoodOrderingSystem.Models.DTO
{
    public class UserPayload
    {
        [MaxLength(255, ErrorMessage = "Too much characters")]
        public string? name { get; set; }

        [MaxLength(255, ErrorMessage = "Too much characters")]
        public string? username { get; set; }
        public int? role {get;set;}
        public string? password { get; set; }
    }
}