
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FoodOrderingSystem.Models.Entities
{
    public class Role
    {
        [Key]
        public int roleId { get; set; }
        [MaxLength(50)]
        public string? name {get;set;}
        public DateTime? created_at { get; set; } = DateTime.Now;
        public DateTime? updated_at { get; set; } = DateTime.Now;
        
        [JsonIgnore]
        public List<User>? users {get;set;}
    }
}