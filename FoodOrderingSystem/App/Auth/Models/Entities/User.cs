
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FoodOrderingSystem.Models.Entities
{
    public class User
    {
        [Key]
        public int userId { get; set; }

        [MaxLength(255, ErrorMessage = "Too much characters")]
        public string? name { get; set; }

        [MaxLength(255, ErrorMessage = "Too much characters")]
        public string? username { get; set; }
        public int? role {get;set;}
        public Role? roleObj {get;set;}

        [JsonIgnore]
        public string? passwordHash { get; set; }
        public string? token { get; set; }
        public DateTime? tokenCreated { get; set; }
        public DateTime? tokenExpires { get; set; }
        public DateTime? created_at { get; set; } = DateTime.Now;
        public DateTime? updated_at { get; set; } = DateTime.Now;
    }
}