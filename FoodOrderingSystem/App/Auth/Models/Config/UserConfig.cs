using Microsoft.EntityFrameworkCore;
using FoodOrderingSystem.Models.Entities;

namespace FoodOrderingSystem.Models.Config
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<User> builder)
        {
            builder
                .HasOne<Role>(s => s.roleObj)
                .WithMany(g => g.users)
                .HasForeignKey(s => s.role)
                .OnDelete(DeleteBehavior.ClientNoAction);
        }
    }
}