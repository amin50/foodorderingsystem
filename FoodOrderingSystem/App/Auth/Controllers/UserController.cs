using FoodOrderingSystem.Models.DTO;
using FoodOrderingSystem.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace FoodOrderingSystem.App.Auth.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private IUser _User;

        public UserController(IUser User)
        {
            _User = User;
        }

        [HttpPost]
        public IActionResult Create(UserPayload body)
        {
            try
            {
                _User.Create(body);

                return Ok(new
                {
                    StatusCode = 200,
                    message = "Success"
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("Error : " + e);
                return BadRequest(new
                {
                    StatusCode = 500,
                    Response = e
                });
            }
        }

        [HttpGet("getone")]
        public IActionResult GetOne(int id)
        {
            try
            {
                var obj = _User.GetOne(id);
                return Ok(new {
                    StatusCode = 200,
                    message = "Get success",
                    Response = obj
                });
            }
            catch(Exception e)
            {
                return BadRequest(new {
                    StatusCode = 500,
                    Response = e
                });
            }
        }
    }
}