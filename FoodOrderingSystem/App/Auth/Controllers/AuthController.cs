using FoodOrderingSystem.Models.DTO;
using FoodOrderingSystem.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace FoodOrderingSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private IUser _User;

        public AuthController(IUser User)
        {
            _User = User;
        }

        [HttpPost("login")]
        public IActionResult Login(LoginPayload body)
        {
            try
            {
                var user = _User.Login(body);

                if (user == null)
                {
                    return NotFound(new
                    {
                        StatusCode = 404,
                        message = "User not found in Application"
                    });
                }

                if (!BCrypt.Net.BCrypt.Verify(body.password, user.passwordHash))
                {
                    return BadRequest(new
                    {
                        StatusCode = 403,
                        message = "Password is incorrect in Application"
                    });
                }

                return Ok(new
                {
                    StatusCode = 200,
                    message = "Login success",
                    Response = user
                });


            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    StatusCode = 500,
                    message = e
                });
            }
        }
    }
}