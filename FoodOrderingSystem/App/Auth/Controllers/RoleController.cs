using FoodOrderingSystem.Models.Entities;
using FoodOrderingSystem.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace FoodOrderingSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RoleController : ControllerBase
    {
        private IUser _User;
        public RoleController(IUser User)
        {
            _User = User;
        }

        [HttpPost]
        public IActionResult Create(Role body)
        {
            try
            {
                _User.CreateRole(body);

                return Ok(new
                {
                    StatusCode = 200,
                    message = "Success"
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("Error : " + e);
                return BadRequest(new
                {
                    StatusCode = 500,
                    Response = e
                });
            }
        }
    }
}