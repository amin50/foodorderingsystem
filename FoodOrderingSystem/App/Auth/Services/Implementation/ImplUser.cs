
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using FoodOrderingSystem.Models.DTO;
using FoodOrderingSystem.Models.Entities;
using FoodOrderingSystem.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using TtsAPI.Config.Data;

namespace FoodOrderingSystem.Services.Implementation
{
    public class ImplUser : IUser
    {
        private DBContext _context;
        private readonly IConfiguration _configuration;
        public ImplUser(DBContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public void Create(UserPayload body)
        {
            var user = new User();

            user.name = body.name;
            user.username = body.username;
            user.passwordHash = BCrypt.Net.BCrypt.HashPassword(body.password);
            user.role = body.role;

            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public void CreateRole(Role body)
        {
            _context.Roles.Add(body);
            _context.SaveChanges();
        }

        public User GetOne(int id)
        {
            var l = _context.Users
                .Include(x=> x.roleObj)
                .SingleOrDefault(x=> x.userId == id);
            return l;
        }

        public User Login(LoginPayload body)
        {
            var user = _context.Users
            .Include(x=> x.roleObj)
            .SingleOrDefault(x => x.username == body.username);
            if (user != null)
            {
                string token = CreateToken(user);

                user.token = token;
                user.tokenCreated = DateTime.Now;
                user.tokenExpires = DateTime.Now.AddDays(1);

                _context.Users.Update(user);
                _context.SaveChanges();

                return user;
            }

            return null;
        }

        private string CreateToken(User user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.username),
                // new Claim(ClaimTypes.Role, user.roleId)
                new Claim(ClaimTypes.Role, user.roleObj.name)
            };

            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(
                _configuration.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: creds);

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            Console.WriteLine(token);

            return jwt;
        }
    }
}