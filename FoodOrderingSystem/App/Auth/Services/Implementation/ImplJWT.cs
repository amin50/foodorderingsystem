
using System.Security.Claims;
using FoodOrderingSystem.Models.Entities;
using FoodOrderingSystem.Services.Interfaces;
using TtsAPI.Config.Data;

namespace FoodOrderingSystem.Services.Implementation
{
    public class ImplJWT: IJWT
    {
        private DBContext _context;

        public ImplJWT(DBContext context)
        {
            _context = context;
        }

        public User VerifyUser(ClaimsIdentity identity)
        {
            if (identity != null)
            {
                var userClaims = identity.Claims;
                var username = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Name)?.Value;
                Console.WriteLine($"Checked userID {username}");
                var user = _context.Users.SingleOrDefault(e => e.username == username);
                return user;
            }

            return null;
        }
    }
}