
using FoodOrderingSystem.Models.DTO;
using FoodOrderingSystem.Models.Entities;

namespace FoodOrderingSystem.Services.Interfaces
{
    public interface IUser
    {
        void Create(UserPayload body);
        void CreateRole(Role body);
        User GetOne(int id);
        User Login(LoginPayload body);
    }
}