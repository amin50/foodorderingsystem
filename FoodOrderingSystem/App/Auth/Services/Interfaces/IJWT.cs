
using System.Security.Claims;
using FoodOrderingSystem.Models.Entities;

namespace FoodOrderingSystem.Services.Interfaces
{
    public interface IJWT
    {
        User VerifyUser(ClaimsIdentity identity);
    }
}