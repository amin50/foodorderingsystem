
using FoodOrderingSystem.Models.Config;
using FoodOrderingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace TtsAPI.Config.Data
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
        }
        public virtual DbSet<Food> Foods { get; set; }
        public virtual DbSet<Drink> Drinks { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<FoodOrder> FoodOrders { get; set; }
        public virtual DbSet<DrinkOrder> DrinkOrders { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Files> Files { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new FoodOrderConfig());
            modelBuilder.ApplyConfiguration(new DrinkOrderConfig());
            modelBuilder.ApplyConfiguration(new UserConfig());
        }
    }
}